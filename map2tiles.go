package main

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
	"path/filepath"
)

var inputImage string
var metatileWidth int
var metatileHeight int
var tileWidth int
var tileHeight int

func validateRequiredProgramInput() {
	err := isInputImageValid()
	abortIfError(err)
}

func readInputImage() {
	processingImage, err := os.Open(inputImage)
	abortIfError(err)
	defer processingImage.Close() // Close image stream at end of method execution

	imageData, imageType, err := image.Decode(processingImage)
	abortIfError(err)

	verifyImageSize(imageData.Bounds())

	fmt.Printf("Image type: %s", imageType)
	fmt.Println()
	fmt.Printf("Color model: %v", imageData.ColorModel())
	fmt.Println()
	fmt.Printf("Bounds: %v", imageData.Bounds())
	fmt.Println()

	// Extract the palette of the screen.
	var uniqueColors []color.Color

	for y := 0; y < imageData.Bounds().Dy(); y += 1 {
		for x := 0; x < imageData.Bounds().Dx(); x += 1 {
			colorCandidate := imageData.At(x, y)
			previouslyFound := false

			for _, uniqueColor := range uniqueColors {
				if uniqueColor == colorCandidate {
					previouslyFound = true
					break
				}
			}

			if !previouslyFound {
				uniqueColors = append(uniqueColors, colorCandidate)
				fmt.Print(colorCandidate)
			}
		}
	}

	// Draw Metatile
	startPoint := image.Pt(0, 0)
	endPoint := image.Pt(metatileWidth, metatileHeight)
	metatileRectangle := image.Rectangle{startPoint, endPoint}

	subImage := imageData.(interface {
		SubImage(r image.Rectangle) image.Image
	}).SubImage(metatileRectangle)

	metatileImage, err := os.Create(filepath.Dir(inputImage) + "/metatile.png")
	abortIfError(err)
	defer metatileImage.Close() // Close image stream at end of method execution

	png.Encode(metatileImage, subImage)
}

func verifyImageSize(bounds image.Rectangle) {
	if bounds.Dx()%metatileWidth != 0 {
		abortWithMessage(fmt.Sprintf("Image width %d must be divisible by metatile width %d", bounds.Dx(), metatileWidth))
	}

	if bounds.Dy()%metatileHeight != 0 {
		abortWithMessage(fmt.Sprintf("Image height %d must be divisible by metatile height %d", bounds.Dy(), metatileHeight))
	}
}

func isInputImageValid() error {
	if inputImage == "" {
		return fmt.Errorf("input flag is required, and cannot be emtpy!")
	}

	return nil
}

func abortIfError(err error) {
	if err != nil {
		abortWithMessage(err.Error())
	}
}

func abortWithMessage(message string) {
	fmt.Println(message)
	os.Exit(1)
}

func main() {
	// Setup input parameter flags.
	flag.StringVar(&inputImage, "input", "", "Required value with the path to the input png image.")
	flag.IntVar(&metatileWidth, "metaWidth", 16, "Metatile width. Default value is 16 px")
	flag.IntVar(&metatileHeight, "metaHeight", 16, "Metatile height. Default value is 16 px")
	flag.IntVar(&tileWidth, "tileWidth", 8, "Tile width. Default value is 8 px")
	flag.IntVar(&tileHeight, "tileHeight", 8, "Tile height. Default value is 8 px")
	flag.Parse()

	validateRequiredProgramInput()
	readInputImage()
}
